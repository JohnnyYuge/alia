% ============================
% ==== Creating the cards ====
% ============================

% The range of cards (goes from the value 1 to 104)
card_range(X):- between(1,104,X).
% Associate the value to the bulls
% 55 is 7, multiple of 11 are 5, multiple of 10 are 3, multiple of 5 are 2, the rest is 1
card_bulls(55,7):- !.
card_bulls(X, 5):- 0 is mod(X,11), !.
card_bulls(X, 3):- 0 is mod(X,10), !.
card_bulls(X, 2):- 0 is mod(X,5), !.
card_bulls(_, 1):- !.

% A card is a value and the bulls associated or an empty space
emptySpace.
card(X,Y):- card_range(X), card_bulls(X,Y).

% Tooling on cards
isGreaterThan(Lhs,Rhs):-
    Lhs = card(Xl,_),
    Rhs = card(Xr,_),
    Xl > Xr.
isRightCardSuperior(Lhs,Rhs):-
    Lhs = card(Xl,_),
    Rhs = card(Xr,_),
    Xl < Xr.
isLesserOrEqualThan(Lhs, Rhs):-
    Lhs = card(Xl,_),
    Rhs = card(Xr,_),
    Xl =< Xr.
% =================
% ==== Sorting ====
% =================

insert_sort(List,Sorted):-i_sort(List,[],Sorted),!.
i_sort([],Acc,Acc).
i_sort([H|T],Acc,Sorted):-insert(H,Acc,NAcc),i_sort(T,NAcc,Sorted).

insert(card(X,Xb),[card(Y,Yb)|T],[card(Y,Yb)|NT]):- X > Y, insert(card(X,Xb),T,NT).
insert(card(X,Xb),[card(Y,Yb)|T],[card(X,Xb),card(Y,Yb)|T]):-X =< Y.
%insert(X,[Y|T],[Y|NT]):- X > Y, insert(X,T,NT).
%insert(X,[Y|T],[X,Y|T]):-X =< Y.
insert(X,[],[X]).

% Scoring a list of cards as a sum of their bulls
deckScore([],0).
deckScore([emptySpace|DeckTail],S):- deckScore(DeckTail,S).
deckScore([DeckHead|DeckTail],S):-
    deckScore(DeckTail,PS),
    DeckHead = card(_,Bulls),
    S is Bulls + PS.

% Take the n first elements of a list
take(N, _, Xs) :- N =< 0, !, N =:= 0, Xs = [].
take(_, [], []).
take(N, [X|Xs], [X|Ys]) :- M is N-1, take(M, Xs, Ys).

%take(N, Ys, Xs, Ys):- N =< 0, !, N =:= 0, Xs = [].
%take(_, [], [], []).
%take(N, [X|Xs], [X|Ys], Xs):- M is N-1, take(M, Xs, Ys, Xs).

% Take the Amount first elements out of a deck of cards (mainly for pro rules)
subdeck(Deck,Amount,SubDeck):-
	take(Amount,Deck, SubDeck).
subdeck(Amount, Deck):- initDeck(ODeck), take(Amount, ODeck, Deck).

% Initialize a full deck (the 104 cards)
initDeck(Deck):-
    findall(card(X,Y),card(X,Y),Deck).
% Shuffle a deck
initShuffledDeck(Deck,ShuffledDeck):- random_permutation(Deck, ShuffledDeck).
initShuffledDeck(ShuffledDeck):- initDeck(TrueDeck), random_permutation(TrueDeck,ShuffledDeck).
initProShuffledDeck(NbPlayers, ShuffledDeck):-
    initDeck(TrueDeck), subdeck(TrueDeck, NbPlayers * 10 + 4, SubDeck), random_permutation(SubDeck, ShuffledDeck).


%dealOne/4
dealOne([],[],[],[]):- !.
dealOne([],FD,[],FD):- !.
dealOne([DH], [], [], [DH]):- !.
dealOne([DH], FD, [], NFD):- append([DH],FD,NFD),!.
dealOne([DH|DT], [], DT,[DH]):- !.
dealOne([DH|DT], FD, DT, NFD):- append([DH],FD,NFD),!.

%dealMultiple/5
dealMultiple([],FD,_,[],FD):- !.
dealMultiple(D,FD,0,D,FD):-!.
dealMultiple(ODeck, OFinalDeck, Times, Deck, FinalDeck):-
    T is Times - 1,
    dealOne(ODeck, OFinalDeck, TDeck, TFinalDeck),
    dealMultiple(TDeck, TFinalDeck, T, Deck, FinalDeck).

% =======================
% ==== HandSplitting ====
% =======================
% doesn't work if the number of hands is not a divider of the number of cards to split
splitInHands(Deck, Hand1, Hand2):-mul2Split(Deck,Hand1,Hand2, 1), !.
splitInHands(Deck, Hand1, Hand2, Hand3):-mul3Split(Deck, Hand1, Hand2, Hand3, 1), !.
splitInHands(Deck, Hand1, Hand2, Hand3, Hand4):-mul4Split(Deck, Hand1, Hand2, Hand3, Hand4, 1), !.

% === 2 Players =====
mul2Split([],[],[],_).
mul2Split([H|T], [H|Odd], Even, 1):- mul2Split(T, Odd, Even, 2).
mul2Split([H|T], Odd, [H|Even], 2):- mul2Split(T, Odd, Even, 1).

% ==== 3 Players ====
mul3Split([],[],[],[],_).
mul3Split([H|T],[H|Hand1],Hand2, Hand3, 1):- mul3Split(T,Hand1,Hand2, Hand3, 2).
mul3Split([H|T],Hand1,[H|Hand2], Hand3, 2):- mul3Split(T,Hand1,Hand2, Hand3, 3).
mul3Split([H|T],Hand1,Hand2, [H|Hand3], 3):- mul3Split(T,Hand1,Hand2, Hand3, 1).

% ==== 4 Players =====
mul4Split([],[],[],[],[],_).
mul4Split([H|T], [H|Hand1], Hand2, Hand3, Hand4, 1):- mul4Split(T,Hand1, Hand2, Hand3, Hand4, 2).
mul4Split([H|T], Hand1, [H|Hand2], Hand3, Hand4, 2):- mul4Split(T,Hand1, Hand2, Hand3, Hand4, 3).
mul4Split([H|T], Hand1, Hand2, [H|Hand3], Hand4, 3):- mul4Split(T,Hand1, Hand2, Hand3, Hand4, 4).
mul4Split([H|T], Hand1, Hand2, Hand3, [H|Hand4], 4):- mul4Split(T,Hand1, Hand2, Hand3, Hand4, 1).

sortDeck(Deck, SortedDeck):- insert_sort(Deck,SortedDeck).
sortDecks([],[]).
sortDecks([X],[Y]):- sortDeck(X,Y).
sortDecks([X|Xs],[Y|Ys]):- sortDeck(X,Y), sortDecks(Xs,Ys).

fill(_, 0, []).
fill(X, N, [X|Xs]) :- succ(N0, N), fill( X, N0, Xs).

% ======================
% ==== Board things ====
% ======================

dropEmptySpaces([],[],0).
dropEmptySpaces([emptySpace|CT],CUP,SpaceDropped):- dropEmptySpaces(CT, CUP,SD), SpaceDropped is SD + 1, !.
dropEmptySpaces([C|CT],[C|CUPT],SpaceDropped):- dropEmptySpaces(CT,CUPT,SpaceDropped).


formatAsBoardRow(Cards,Row):-
    dropEmptySpaces(Cards,CleanedUpCards,_),
    length(CleanedUpCards,L),
    L =< 5,
    Filler is 5 - L,
    fill(emptySpace, Filler, EmptySpaces),
    append(CleanedUpCards, EmptySpaces,Row).



%isBoardRow(Row):- formatAsBoardRow( Row, Row ).

isRowFull( Row ):-
    dropEmptySpaces( Row, CRow, _ ),
    length( CRow, L ),
    L = 5.

rightMostCard( Row, Card ):-
    dropEmptySpaces(Row, NRow, _), reverse( NRow, ReversedNRow ), append([Card], _, ReversedNRow).

queueCardInRow(ORow, Card, Row ):-
    dropEmptySpaces(ORow,OCURow,_),
    append(OCURow,[Card],NRow),
    formatAsBoardRow(NRow,Row).

lowestRowDifference( [], _, null, inf).
lowestRowDifference( [X|Xs], card(CV, CB), Row, Diff ):-
    rightMostCard( X, card(V, _) ),
    LocalDiff is CV - V,
    lowestRowDifference( Xs, card(CV, CB), DownRow, DownDiff ),
    (
    	LocalDiff > 0 ->
    	(
        	DownDiff < LocalDiff ->
        	Row = DownRow, Diff = DownDiff;
        	Row = X, Diff = LocalDiff
        );
    	Row = DownRow, Diff = DownDiff
    ).

lowestPlayableRow( [], null, inf).
lowestPlayableRow( [X|Xs], Row, LowestRight ):-
    rightMostCard( X, card(V, _) ),
    lowestPlayableRow( Xs, DownRow, DownLowest ),
    (
    	DownLowest < V ->
    	Row = DownRow, LowestRight = DownLowest;
    	Row = X, LowestRight = V
    ).

lowestRowScore( [], null, inf).
lowestRowScore( [R|Rows], Row, LowestScore):-
    deckScore( R, Score ),
	lowestRowScore( Rows, DownRow, DownLS ),
    (
    	DownLS < Score ->
    	Row = DownRow, LowestScore = DownLS;
    	Row = R, LowestScore = Score
    ), !.

replace( _, [], _, []).
replace( ORow, [ORow|Rs], Row, [Row|Ns] ):- replace( ORow, Rs, Row, Ns ), !.
replace( ORow, [R|Rs], Row, [R|Ns] ):- replace( ORow, Rs, Row, Ns ), !.



initEmptyBoard(Board):-
    Board = [Row1, Row2, Row3, Row4],
	formatAsBoardRow([],Row1),
    formatAsBoardRow([],Row2),
    formatAsBoardRow([],Row3),
    formatAsBoardRow([],Row4).

initFirstColumnBoard([C1,C2,C3,C4], Board):-
    Board = [Row1, Row2, Row3, Row4],
	formatAsBoardRow([C1],Row1),
    formatAsBoardRow([C2],Row2),
    formatAsBoardRow([C3],Row3),
    formatAsBoardRow([C4],Row4).



% ==== Displays ====

show( emptySpace ):- write("[ . ]").
show( card(X,_) ):- X < 10, write("[ "), write(X), write(" ]"), !.
show( card(X,_) ):- X < 100, write("[ "), write(X), write(" ]"), !.
show( card(X,_) ):- write("[ "), write(X), write(" ]").
showRow( [] ).
showRow( [X|Xs] ):- show(X), write("\t\t"), showRow( Xs ).
showBoard( [] ).
showBoard( [Row|Rows] ):- showRow( Row ), deckScore(Row, Score), write(" Score "), writeln( Score), showBoard( Rows ), !.


% ================================
% ==== The game in itself !!! ====
% ================================

init( Rest, Hands, Board, Discards ):-
    NbPlayers = 2,
    initProShuffledDeck(NbPlayers, Deck),
    dealMultiple(Deck, [], NbPlayers * 10, NotSortedRestDeck, DistributeDeck),
    sortDeck(NotSortedRestDeck, RestDeck),
    splitInHands(DistributeDeck, Hand1, Hand2),
    sortDecks([Hand1, Hand2], Hands),
    length(FirstColumn,4),
    append(FirstColumn, Rest, RestDeck),
    initFirstColumnBoard(FirstColumn, Board),
    fill([], NbPlayers, Discards),
    !.


% ==== Playout ====

% There is no row fitting the rule "the card played should be greater than every over card in the row"
resolvePlayout( Board, Card, Discards, ResultBoard, ResultDiscards ):-
    lowestRowDifference( Board, Card, Row, _),
    Row = null,
    lowestRowScore( Board, LSRow, _ ),
    dropEmptySpaces( LSRow, RowDiscards, _),
    append( Discards, RowDiscards, ResultDiscards ),
    queueCardInRow( [], Card, NewRow ),
    replace( LSRow, Board, NewRow, ResultBoard ), !.

% There is a row fitting this rule but the row is full
resolvePlayout( Board, Card, Discards, ResultBoard, ResultDiscards ):-
    lowestRowDifference( Board, Card, Row, _),
    isRowFull( Row ),
    dropEmptySpaces( Row, RowDiscards, _),
    append( Discards, RowDiscards, ResultDiscards ),
    queueCardInRow( [], Card, NewRow ),
    replace( Row, Board, NewRow, ResultBoard ),

    !.

% It's perfectly fine
resolvePlayout( Board, Card, Discards, ResultBoard, ResultDiscards ):-
    lowestRowDifference( Board, Card, Row, _),
    Discards = ResultDiscards,
    queueCardInRow( Row, Card, NewRow ),
    replace( Row, Board, NewRow, ResultBoard ), !.

% Final recursion
playout( [], Board, Board, [], [] ):- !.
% Last card recursion
playout( [ C ], Board, [ D ], NewBoard, [ ND ] ):-
    resolvePlayout( Board, C, D, NewBoard, ND ).

% Card 1 sup Card 2 recursion
playout( [ C1, C2 | Cards ], Board, [ D1, D2 | Discards ], NewBoard, [ ND1, ND2 | NDiscards ] ):-
    C1 = card(V1, _), C2 = card(V2, _),
	V2 > V1,
    playout( [ C1 | Cards ], Board, [ D1 | Discards], IntermediateBoard, [ ND1 | NDiscards ] ),
    resolvePlayout( IntermediateBoard, C2, D2, NewBoard, ND2 ).


% Card 2 sup Card 1 recursion
playout( [ C1, C2 | Cards ], Board, [ D1, D2 | Discards ], NewBoard, [ ND1, ND2 | NDiscards ] ):-
    C1 = card(V1, _), C2 = card(V2, _),
	V1 > V2,
    playout( [ C2 | Cards ], Board, [ D2 | Discards], IntermediateBoard, [ ND2 | NDiscards ] ),
    resolvePlayout( IntermediateBoard, C1, D1, NewBoard, ND1 ).


% ==== Winner ====

displayWinner( [], _, inf ):- !.
displayWinner( [ D | Ds ], Rank, Score ):-
	deckScore( D, Score ),
    DownRank is Rank + 1,
    write(" == Player "), write(Rank), write(" scored "), write(Score), write(" points"), writeln(" =="),
    displayWinner( Ds, DownRank, _ ),
    !.

displayWinner( Discards ):-
    writeln("== [] == [] == [] == [] == [] =="),
    displayWinner( Discards, 1, _),
    writeln("== [] == [] == [] == [] == [] ==").


% ==== AI ====

firstCardGreaterThan( [], _, card(104,0) ):- !.
firstCardGreaterThan( [ card(V,B)|Ds], Value, card(V, B) ):-
    firstCardGreaterThan( Ds, Value, card(BV, _) ),
    between( Value, BV, V ),
    !.
firstCardGreaterThan( [ card(V,_)|Ds], Value, card(BV,BB) ):-
    firstCardGreaterThan( Ds, Value, card(BV, BB) ),
    between( Value, V, BV ),
    !.
firstCardGreaterThan( [ card(V,_)|Ds], Value, card(BV,BB) ):-
    firstCardGreaterThan( Ds, Value, card(BV, BB) ),
    between( V, BV, Value ),
    !.

% == Atoms for AIs ==
stupid_ai.
standard_ai.

% given up on this because Prolog is kinda under optimised for this game
% https://github.com/jaunerc/minimax-prolog/blob/master/minimax.pl for insipiration
intelligent_ai. 

play( stupid_ai, Hand, _, _, PlayedCard):-
    stupidAi( Hand, PlayedCard ).

play( standard_ai, Hand, Board, _, PlayedCard):-
    standardAi( Hand, Board, PlayedCard ).

stupidAi( Hand, PlayedCard ):-
    random_select( PlayedCard, Hand, _).

standardAi( Hand, Board, PlayedCard ):-
	lowestPlayableRow( Board, _, Score ),
    firstCardGreaterThan( Hand, Score, card(104,0 ) ),
    random_select( PlayedCard, Hand, _), !.

standardAi( Hand, Board, PlayedCard ):-
	lowestPlayableRow( Board, _, Score ),
    firstCardGreaterThan( Hand, Score, PlayedCard ),
    !.


% Two players only so far
playATurn( [ AI1, AI2 ], Hands, Board, Discards, NewHands, NewBoard, NewDiscards ):-
    Hands = [Hand1, Hand2],
    Discards = [Discards1, Discards2],
    play( AI1, Hand1, Board, Discards1, C1 ),
    play( AI2, Hand2, Board, Discards2, C2 ),
    playout( [C1, C2], Board, Discards, NewBoard, NewDiscards ),
    select( C1, Hand1, NHand1 ),
    select( C2, Hand2, NHand2 ),
    NewHands = [ NHand1, NHand2 ].

playUntilTheEnd( _, [[],[]], Board, Discards, [[],[]], Board, Discards, _):-
    %writeln("Game ended"),
    !.
playUntilTheEnd( AIs, Hands, Board, Discards, EndHands, EndBoard, EndDiscards, Turn ):-
    NTurn is Turn + 1,
    %showBoard( Board ),
    playATurn( AIs, Hands, Board, Discards, NewHands, NewBoard, NewDiscards ),
    playUntilTheEnd( AIs, NewHands, NewBoard, NewDiscards, EndHands, EndBoard, EndDiscards, NTurn ),
    !.

playUntilTheEnd( AIs, Hands, Board, Discards, EndHands, EndBoard, EndDiscard ):-
    playUntilTheEnd( AIs, Hands, Board, Discards, EndHands, EndBoard, EndDiscard, 1 ).

playMultipleGames( _, 0, [], [], 0 ):- !.
playMultipleGames( AIs, Count, [ S1 | R1 ], [ S2 | R2 ], W1 ):-
    init(_, Hands, Board, Discards ),
    playUntilTheEnd( AIs, Hands, Board, Discards, _, _, NewDiscards ),
    NewDiscards = [ Ds1, Ds2 ],
    deckScore( Ds1, S1 ),
    deckScore( Ds2, S2 ),
    NCount is Count - 1,
    playMultipleGames( AIs, NCount, R1, R2, PW1 ),
    (   S1 < S2 -> W1 is PW1 + 1; W1 is PW1 ),
    !.

playMultipleGames( AIs, Count ):-
    playMultipleGames( AIs, Count, R1, R2, W1 ),
    sum_list( R1, S1 ),
    sum_list( R2, S2 ),
    Perc is W1 * 100 / Count,
    write("Total score Player 1 : "), writeln( S1 ),
    write("Total score Player 2 : "), writeln( S2 ),
    write("On "), write( Count ), write(" games, Player one won "), write( Perc ), writeln(" % of them").

/*
init(_, Hands, Board, Discards ),
Hands = [H|_],
showBoard( Board ),playUntilTheEnd( Hands, Board, Discards, NewHands, NewBoard, NewDiscards ),
writeln("- - - - - - - - - - - - - - - - - - - - -"),
showBoard( NewBoard ),
displayWinner( NewDiscards ),
firstCardGreaterThan( H, 24, Card ),
!.
*/
