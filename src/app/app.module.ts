import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http"

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardComponent } from './components/card/card.component';
import { TimesPipe } from './pipes/times-pipe.pipe';
import { BoardComponent } from './components/board/board.component';
import { HandComponent } from './components/hand/hand.component';

import { AiService } from "./services/ai.service"

@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    TimesPipe,
    BoardComponent,
    HandComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    AiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
