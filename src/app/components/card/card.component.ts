import { Component, OnInit, Input } from '@angular/core';
import { Card } from "../../models/card"

@Component({
  selector: 'playing-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input()
  card: Card = new Card(0)

  @Input()
  empty: boolean = false

  constructor() {

  }

  ngOnInit() {
  }

}
