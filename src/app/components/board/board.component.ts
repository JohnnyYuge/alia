import { Component, OnInit, Input } from '@angular/core';
import { Card } from "../../models/card"

export type Board = [Array<Card>,Array<Card>,Array<Card>,Array<Card>]

@Component({
  selector: 'board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {

  @Input()
  board: Board = [ [], [], [], [] ]

  constructor() { }

  ngOnInit() {}

}
