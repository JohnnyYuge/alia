import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Card } from "../../models/card"

export type Hand = Array<Card>

const RAY = 2000

@Component({
  selector: 'hand',
  templateUrl: './hand.component.html',
  styleUrls: ['./hand.component.css']
})
export class HandComponent implements OnInit {

  @Input()
  hand: Hand = []

  @Output()
  cardSelect = new EventEmitter<Card>()

  @Input()
  cardSelected: Card = null

  constructor() { }

  ngOnInit() {
  }

  select(card: Card) {
    if(this.cardSelected == card) this.cardSelected = null
    else  this.cardSelected = card
    this.cardSelect.emit( this.cardSelected )
  }

  private computeDegree(index: number, length: number) {
    let maxDegree = Math.round( Math.asin( 500 / RAY ) / Math.PI * 180 )
    let degreeByIndex = maxDegree * 2 / length + -0
    let degree = -degreeByIndex * (index - (length-1)  / 2 )

    return degree
  }

  computeRotation(index: number, length: number) {
    return 'rotate(' + this.computeDegree(index, length) + "deg)"
  }

  computeLeft(index: number, length: number) {
    let degree = this.computeDegree(index, length)
    let pos = Math.sin(degree * Math.PI / 180) * RAY

    return pos + 'px'
  }

  computeBottom(index: number, length: number) {
    let degree = this.computeDegree(index, length)
    let pos = -RAY - 180 + Math.cos(degree * Math.PI / 180) * RAY

    return pos + 'px'
  }

}
