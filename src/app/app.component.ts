import { Component } from '@angular/core';
import { Card } from "./models/card"
import { SixTakingGame } from "./models/six-taking-game"
import { AiService } from "./services/ai.service"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  game: SixTakingGame = new SixTakingGame()
  cards: Array<Card> = []

  playerIndex: number = 0
  activeHand: Array<Card> = null

  selectedCards: Array<{card: Card, hand: Array<Card>, handIdx: number}> = []

  constructor( private ai: AiService ) {
    for(let i = 1; i <= 104; i++) {
      this.cards.push( new Card(i))
    }

    this.start()
  }

  start() {
    this.game.start()
    this.activeHand = this.game.hands[ this.playerIndex ]
    this.selectedCards = []
    for(let i = 0; i < this.game.numberOfPlayers; i++)  this.selectedCards.push(null)
  }

  cycleHand() {
    this.playerIndex += 1
    this.playerIndex %= this.game.numberOfPlayers
    this.activeHand = this.game.hands[ this.playerIndex ]
  }

  onSelectedCard(card: Card, handIdx: number) {
    this.selectedCards[handIdx] = (card ? {card: card, hand: this.game.hands[handIdx], handIdx: handIdx } : null)
    console.log(card, handIdx)
  }

  get canPlayTurn(): boolean {
    for(let sc of this.selectedCards) if(!sc) return true
    return false
  }

  playTheTurn() {
    this.game.playATurn( this.selectedCards )
  }
}
