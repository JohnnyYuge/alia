import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http"

declare var pl;

@Injectable({
  providedIn: 'root'
})
export class AiService {

  private httpOptions: any = { responseType: 'text' }

  constructor(private http: HttpClient ) {
    let session = pl.create()
    this.http.get("assets/prolog/deck.pl", this.httpOptions )
      .subscribe(
        r => {
          console.log( r )
          let program = session.consult( r )
          console.log(program)
          session.query("init(RD,Hands).")
          session.answer(console.log)
        },

        error => {
            console.error( error )
        }
      )
    console.log( pl, session )
  }

  private generateDeck() {
    let cardsPredicate = "";
    let deck = []
    for(let i = 1; i <= 104; i++) {
      let y = 1
      if(i == 55)           y = 7
      else if(i % 11 == 0)  y = 5
      else if(i % 10 == 0)  y = 3
      else if(i % 5 == 0)   y = 2
      else                    y = 1
      let card = `card(${i},${y})`
      cardsPredicate += card + `.\n`
      deck.push(card)
    }

    cardsPredicate += `\ninitDeck(Deck):- Deck = [${deck.join(",")}].\n`
    cardsPredicate += "initShuffledDeck(ShuffledDeck):- initDeck(TrueDeck), random_permutation(TrueDeck,ShuffledDeck)."
    console.log(cardsPredicate)
    return cardsPredicate
  }
}
