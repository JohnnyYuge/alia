import { Card } from "./card"

export type Hand = Array<Card>
export type Deck = Array<Card>
export type Board = [Array<Card>,Array<Card>,Array<Card>,Array<Card>]

export class SixTakingGame {


  hands: Array<Hand> = []
  discards: Array<Array<Card>> = []
  scores: Array<number> = []
  deck: Deck = []
  board: Board = [ [], [], [], [] ]

  numberOfCardDistributed: number = 10
  numberOfPlayers: number = 4
  private _turn: number = 0
  get turn() {
    return this._turn
  }

  constructor() {

  }

  private refillDeck() {
    this.deck = []
    for(let i = 0; i < 104; i++) {
      this.deck.push( new Card(i+1) )
    }
  }

  private shuffleDeck() {
    for(let i = this.deck.length; i > 0; i--) {
      let j = Math.floor(Math.random() * (i + 1) )
      let swap = this.deck[i]
      this.deck[i] = this.deck[j]
      this.deck[j] = swap
    }
  }

  private distributeCards() {

    this.hands    = []
    this.discards = []
    this.scores   = []

    for(let i = 0; i < this.numberOfPlayers; i++) {
      this.hands.push([])
      this.discards.push([])
      this.scores.push(0)
    }

    for(let i = 0; i < this.numberOfCardDistributed; i++) {
      for(let h of this.hands) {
        h.push(this.deck.pop())
      }
    }

    for(let h of this.hands) {
      h.sort((lhs, rhs) => { return rhs.value - lhs.value })
    }
  }

  private prepareBoard() {
    for(let i = 0; i < 4; i++) {
      this.board[i] = [this.deck.pop()]
    }
  }

  private detectEnd() {
    if(this.turn >= this.numberOfCardDistributed) {
      this.onGameEnd( this.scores )
    }
  }

  onGameEnd( scores: Array<number> ) {
    console.log("The game has ended")
  }

  start( nbPlayers: number = 2, nbCards: number = 10 ) {
    this.numberOfPlayers = nbPlayers
    this.numberOfCardDistributed = nbCards
    this._turn = 0

    this.refillDeck()
    this.shuffleDeck()
    this.distributeCards()
    this.prepareBoard()
  }

  playATurn( selectedCards: Array<{card: Card, hand: Array<Card>, handIdx: number}> ) {
    this._turn += 1
    // Order the cards to play them in turn from lowest to greatest value
    selectedCards.sort(
      (lhs,rhs) => {
        return lhs.card.value - rhs.card.value
      }
    )

    // Check where each card should be played
    for(let sc of selectedCards) {
      let rowPicked = -1
      let rowMinimalDif = 104

      for(let i = 0; i < 4; i++) {
        let val = this.board[i][this.board[i].length-1].value
        let dif = sc.card.value - val

        if(dif <= 0) continue

        if(dif < rowMinimalDif) {
          rowMinimalDif = dif
          rowPicked = i
        }
      }

      // If it can be played, play it
      if(rowPicked > -1) {
        if(this.board[rowPicked].length == 5) {
          while(this.board[rowPicked].length) {
            this.discards[sc.handIdx].push( this.board[rowPicked].pop() )
          }
          this.board[rowPicked].length = 0
        }
        this.board[rowPicked].push( sc.card )
      }
      // If it can't be played
      else {
        let bScore = 6 * 7
        let boardTR = -1
        for(let i = 0; i < 4; i++) {
          let score = 0
          for(let card of this.board[i]) {
            if(!card) break
            score += card.bulls
          }
          if(score < bScore) {
            bScore = score
            boardTR = i
          }
        }

        while(this.board[boardTR].length) {
          this.discards[sc.handIdx].push( this.board[boardTR].pop() )
        }

        this.board[boardTR] = [sc.card]
      }


      // Remove the cards from the hands
      for(let i = 0; i < this.hands[sc.handIdx].length; i++) {
        if( sc.card == this.hands[sc.handIdx][i] ) {
          this.hands[sc.handIdx].splice(i,1)
          break
        }
      }
    }

    // Compute the scores
    for(let i = 0; i < this.numberOfPlayers; i++) {
      this.scores[i] = 0
      for(let card of this.discards[i]) {
        this.scores[i] += card.bulls
      }
    }

    // Deselect cards
    for(let i = 0; i < selectedCards.length; i++) {
      selectedCards[i] = null
    }

    this.detectEnd()
  }
}
