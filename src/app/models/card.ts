export class Card {
  private _value: number
  private _bulls: number

  constructor(value) {
    this.value = value
  }

  set value(val: number) {
    this._value = val
    if(val == 55)           this._bulls = 7
    else if(val % 11 == 0)  this._bulls = 5
    else if(val % 10 == 0)  this._bulls = 3
    else if(val % 5 == 0)   this._bulls = 2
    else if(val == 0)       this._bulls = 0
    else                    this._bulls = 1
  }

  get value() {
    return this._value
  }

  get bulls() {
    return this._bulls
  }
}
